import React, { useEffect, useState } from "react";

import Layout from "./components/Layout/Layout";
import Sidebar from "./components/Sidebar/Sidebar";
import DisplayArea from "./components/DisplayArea/DisplayArea";
import "./App.css";
import initialUsers from "./assets/users";

function App() {
  const [users, setUsers] = useState([]);
  const [filteredUsers, setFilteredUsers] = useState(null);
  const [displayedUsers, setDisplayedUsers] = useState([]);
  const [error, setError] = useState(false);

  // Getting initial users
  useEffect(() => {
    const intialUsersWithColors = initialUsers.map(user => {
      // Setting random colors for users without photo
      if (!user.photoUrl) {
        user.color = Math.random()
          .toString(16)
          .slice(2, 8);
      }
      return user;
    });
    setUsers(intialUsersWithColors);
  }, []);

  // Setting active user based on index
  const handleSetActiveUser = userIndex => {
    setError(false);
    const userToActivate = filteredUsers
      ? filteredUsers[userIndex]
      : users[userIndex];
    if (displayedUsers.includes(userToActivate)) {
      setError("User is already included");
      return;
    }
    setDisplayedUsers(displayedUsers => [...displayedUsers, userToActivate]);
  };

  // Deactivating user based on index
  const handleDeactivateUser = userIndex => {
    setError(false);
    const tempUsers = [...displayedUsers];
    tempUsers.splice(userIndex, 1);
    setDisplayedUsers(tempUsers);
  };

  // Filtering by name and surname
  const handleFiltering = input => {
    const filtered = users.filter(user => {
      const name = user.name.toLowerCase();
      const surname = user.surname.toLowerCase();
      const check = input.trim().toLowerCase();

      return (
        name.includes(check) ||
        surname.includes(check) ||
        (name + " " + surname).includes(check)
      );
    });
    setFilteredUsers(filtered);
  };

  // Deleting user permanently from both filtered and non-filtered array
  // const handleDeleteUser = userInfo => {
  //   const newUsers = users.filter(
  //     user => user.name !== userInfo.name && user.surname !== userInfo.surname
  //   );
  //   if (filteredUsers) {
  //     const newFilteredUsers = filteredUsers.filter(
  //       user => user.name !== userInfo.name && user.surname !== userInfo.surname
  //     );
  //     setFilteredUsers(newFilteredUsers);
  //   }
  //   setUsers(newUsers);
  // };

  return (
    <Layout>
      <Sidebar
        users={filteredUsers ? filteredUsers : users}
        setActiveUser={handleSetActiveUser}
        filter={handleFiltering}
      />
      <DisplayArea
        users={displayedUsers}
        deleteUser={handleDeactivateUser}
        error={error}
      />
    </Layout>
  );
}

export default App;
