import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Col } from "react-bootstrap";

import DisplayItem from "./DisplayItem/DisplayItem";

const DisplayArea = ({ users, deleteUser, error }) => {
  const bottom = useRef(null);

  const scrollToBottom = () => {
    bottom.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(scrollToBottom, [users, error]);

  return (
    <Wrapper sm={6} md={8}>
      {users.length > 0 ? (
        <>
          {users.map((user, index) => (
            <DisplayItem
              user={user}
              key={index}
              deleteUser={deleteUser}
              index={index}
            />
          ))}
        </>
      ) : (
        <h2>Please choose a user</h2>
      )}
      {error && <h2>{error}</h2>}
      <div ref={bottom} />
    </Wrapper>
  );
};

const Wrapper = styled(Col)`
  margin-top: 2rem;
  height: 90%;
  overflow-y: auto;
`;

DisplayArea.propTypes = {
  users: PropTypes.array.isRequired,
  deleteUser: PropTypes.func.isRequired,
  error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool])
};

export default DisplayArea;
