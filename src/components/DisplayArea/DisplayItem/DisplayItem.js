import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Col, Row } from "react-bootstrap";

import PhotoContainer from "../../common/PhotoContainer";

const DisplayItem = ({ user, deleteUser, index }) => {
  const handleDelete = () => {
    deleteUser(index);
  };

  return (
    <RowWrapper>
      <Col md={6} lg={4}>
        <PhotoContainer user={user} />
      </Col>
      <Col md={6} lg={8}>
        <InfoContainer>
          <div>
            <h1>
              {user.name} {user.surname}
            </h1>
            <h3>{user.email}</h3>
            <button onClick={handleDelete} className="btn btn-danger">
              Delete user
            </button>
          </div>
        </InfoContainer>
      </Col>
    </RowWrapper>
  );
};

const RowWrapper = styled(Row)`
  margin-bottom: 1rem;
`;

const InfoContainer = styled.div`
  display: flex;
  align-items: center;

  h3 {
    word-break: break-all;
  }
`;

DisplayItem.propTypes = {
  user: PropTypes.object.isRequired,
  deleteUser: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired
};

export default DisplayItem;
