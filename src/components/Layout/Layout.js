import React from "react";
import PropTypes from "prop-types";
import { Container, Row } from "react-bootstrap";
import styled from "styled-components";

const Layout = ({ children }) => {
  return (
    <Container>
      <RowWrapprer>{children}</RowWrapprer>
    </Container>
  );
};

const RowWrapprer = styled(Row)`
  height: 100vh;
  padding-top: 2rem;
`;

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
