import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const Filter = ({ filter }) => {
  const handleInput = e => {
    filter(e.target.value);
  };

  return (
    <Wrapper>
      <i className="fa fa-search"></i>
      <input type="text" onChange={handleInput} placeholder="Find user" />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100%;
  margin-bottom: 1rem;

  .fa-search {
    position: absolute;
    top: 6px;
    left: 20px;
  }

  input {
    padding-left: 25px;
    border: 1px solid darkgray;
  }

  input:focus {
    outline: none;
  }
`;

Filter.propTypes = {
  filter: PropTypes.func.isRequired
};

export default Filter;
