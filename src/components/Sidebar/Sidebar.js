import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Col } from "react-bootstrap";

import Filter from "./Filter/Filter";
import SidebarItem from "./SidebarItem/SidebarItem";

const Sidebar = ({ users, setActiveUser, filter }) => {
  // Setting up display property to toogle bootstrap display classes
  const [display, setDisplay] = useState(true);

  // Toggling displaying sidebar
  const handleToggle = () => {
    setDisplay(display => !display);
  };

  return (
    <>
      <Wrapper
        sm={6}
        md={4}
        // Displaying only when display is true or screen is bigger than xs
        className={display ? "d-block" : "d-none d-sm-block"}
      >
        <Filter filter={filter} />
        {users.length > 0 ? (
          <Items>
            {users.map((user, index) => (
              <SidebarItem
                key={index}
                user={user}
                setActive={setActiveUser}
                index={index}
                toogleSidebar={handleToggle}
              />
            ))}
          </Items>
        ) : (
          <h3>No users found...</h3>
        )}
      </Wrapper>
      <Toggler
        onClick={handleToggle}
        // Displaying on xs screen only
        className="d-block d-sm-none"
      >
        <i className="fa fa-search"></i>
      </Toggler>
    </>
  );
};

const Toggler = styled.div`
  position: absolute;
  left: 1rem;
`;

const Items = styled.div`
  height: 90%;
  overflow-y: auto;
`;

const Wrapper = styled(Col)`
  height: 100%;
  position: absolute;
  z-index: 1;
  background-color: white;

  @media (min-width: 576px) {
    position: relative;
  }
`;

Sidebar.propTypes = {
  users: PropTypes.array.isRequired,
  setActiveUser: PropTypes.func.isRequired,
  filter: PropTypes.func.isRequired
};

export default Sidebar;
