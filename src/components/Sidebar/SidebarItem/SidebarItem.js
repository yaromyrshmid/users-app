import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

import PhotoContainer from "../../common/PhotoContainer";

const SidebarItem = ({ user, index, setActive, toogleSidebar }) => {
  const handleSetActive = () => {
    setActive(index);
    toogleSidebar();
  };

  return (
    <Wrapper onClick={handleSetActive}>
      <PhotoContainer user={user} small />
      <NameContainer>
        <h5>
          {user.name} {user.surname}
        </h5>
      </NameContainer>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-shrink: 0;
  margin-bottom: 1rem;
  cursor: pointer;
`;

const NameContainer = styled.div`
  width: 190px;
  display: flex;
  align-items: center;

  @media (min-width: 576px) {
    width: 130px;
  }

  @media (min-width: 992px) {
    width: 190px;
  }
`;

SidebarItem.propTypes = {
  user: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  setActive: PropTypes.func.isRequired,
  toogleSidebar: PropTypes.func.isRequired
};

export default SidebarItem;
