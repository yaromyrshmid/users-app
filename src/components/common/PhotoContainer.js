import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const PhotoContainer = ({ user, small }) => {
  const { photoUrl, name, surname, color } = user;

  return (
    <>
      {photoUrl ? (
        <ImageContainer small={small}>
          <img src={photoUrl} alt={`${name} ${surname}`} />
        </ImageContainer>
      ) : (
        <ImageContainer color={color} small={small}>
          <h2>{name[0] + surname[0]}</h2>
        </ImageContainer>
      )}
    </>
  );
};

const ImageContainer = styled.div`
  border-radius: 50%;
  width: ${props => (props.small ? "70px" : "200px")};
  height: ${props => (props.small ? "70px" : "200px")};
  overflow: hidden;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-right: 1rem;
  background-color: ${props => (props.color ? `#${props.color}` : "white")};

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  h2 {
    text-align: center;
    text-transform: uppercase;
  }
`;

PhotoContainer.propTypes = {
  user: PropTypes.object.isRequired,
  small: PropTypes.bool
};

export default PhotoContainer;
